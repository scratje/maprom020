#define	VERSION		1
#define	REVISION	13
#define	DATE	"24.06.2024"
#define	VERS	"maprom 1.13"
#define	VSTRING	"maprom 1.13 (24.06.2024)\n\r"
#define	VERSTAG	"\0$VER: maprom 1.13 (24.06.2024)"
