# MapROM020

MapROM utility for Matze's TK68EC020, the 68030-SD-RAM-Board, the TK68030V2 and hamags "Dicke Olga".
It configures the IDE-waitstates (all cards) and MapRom-functionality (TK68EC020, TK68030V2). 

Please download the executable "MapROM", if you are not interested in the surce code and just want to use it ;) .

Parameters:

* KICKROM: Load the build in ROM into Fastram and enables it and performs a reset, if necessary 
* VBR2FAST: Load the interrupt vector base register into fastram. Even more speed!
* KICKFILE 'file' : Load the specified file as kickstart to F8 and performs a reset
* EXTFILE 'file' : Load the specified file as extended-ROM to E0 (CDTV und 1MB ROMs) and performs a reset
* DIAGFILE 'file' : Load the specified file as F0-ROM and performs a reset
* FORCE : Ignores writeprotected ROM-areas
* REMOVE : Removes all ROM-overlays (like a coldstart)
* STATUS : returns the status
* WAITSTATE 'number' : sets the number of waitstates (0-15) for the IDE-interface. The default (15) is very slow and a 'WAITSTATE 1' is tollerated by most CF-cards
* FUSE 'hex' : sets r/w-fuses (see below)
* QUIET : No output but "errorlevel 5", when no card is found

The fuses (pro only, will be set automatically, but you can use this to enable certain regions to become RAM)
0x1000 ENABLE_F8_READ_ENABLE
0x2000 ENABLE_F8_WRITE_PROTECT
0x4000 ENABLE_E0_READ_ENABLE (for 1MB/CDTV ROMs also enables F0-region) 
0x8000 ENABLE_E0_WRITE_PROTECT (for 1MB/CDTV ROMs also disables F0-region)

If you want RAM at E0 and at F0:
MapROM FUSE 0x4000

to allow read access
 You can add the fuses hexadecimal to combine them:
0x4000+0x8000 = 0xC000 (Hex: 10 = A, 11= B, 12=C, 13=D, 14=E ,15 = F)
