/*
 * MapROM utility for Matze`s 68020 CPU card
 *
 * 2019-03-19	chr	Init
 * 2019-04-26	chr	Removed KillExec stuff, added reset code
 * 2019-05-05	chr	Added VBR2FAST and FORCE options	
 *
 */

#include <exec/exec.h>
#include <exec/execbase.h>
#include <dos/dos.h>
#include <exec/libraries.h>
#include <libraries/expansion.h>
#include <libraries/expansionbase.h>
#include <proto/expansion.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "maprom_rev.h"


/* Global vars (comand line params)*/

BOOL quiet = FALSE;
BOOL force = FALSE;
BOOL status = FALSE;
BOOL removeFuses = FALSE;
BOOL vbr2fast = FALSE;
BOOL kickrom = FALSE;
BOOL setWaitState = FALSE;
BOOL setFuseMap = FALSE;
BOOL is68030TKV2 = FALSE;
BOOL isFatOlga = FALSE;
BOOL is68EC020 = FALSE;
UWORD fuseMap = 0;
UWORD waitState = 0;
STRPTR kickFile = NULL;
STRPTR extFile =NULL;
STRPTR diagFile = NULL;


const char *version = VERSTAG;

/* ReadArgs stuff */

#define CMD_TEMPLATE	"WAITSTATE/N,FUSE/K,KICKFILE/K,EXTFILE/K,DIAGFILE/K,KICKROM/S,VBR2FAST/S,REMOVE/S,STATUS/S,QUIET/S,FORCE/S"
#define ARG_WAITSTATE	0
#define ARG_FUSE	1
#define ARG_KICKFILE	2
#define ARG_EXTFILE	3
#define ARG_DIAGFILE	4
#define ARG_KICKROM	5
#define ARG_VBR2FAST	6
#define ARG_REMOVE	7
#define ARG_STATUS	8
#define ARG_QUIET	9
#define ARG_FORCE	10
#define ARG_COUNT	11
long *argvalues[ARG_COUNT] = { NULL };


#define MAX_WAIT 0x000F // maximum allowed wait states
#define ENABLE_F8_READ_ENABLE 0x1000
#define ENABLE_F8_WRITE_PROTECT 0x2000
#define ENABLE_E0_READ_ENABLE 0x4000
#define ENABLE_E0_WRITE_PROTECT 0x8000
#define MANUFACTOR 2092
#define PRODUCT 6
#define MANUFACTOR_A1K 0x0A1C
#define PRODUCT_MATZE_IDE 0x7D
#define PRODUCT_TK68EC020 8
#define PRODUCT_TK68030V2 15
#define PRODUCT_DICKE_OLGA 0xD0
#define SERIAL 0xB16B00B5 //dicke ???
#define SERIAL_TK68030V2 6 // config_bits supported from fw 6!
#define SERIAL_DICKE_OLGA 6 // config_bits supported from fw 6!
#define SERIAL_TK68EC020_DIFFERENT_BITMAP 4 //from FW ver 4 the fusemap is set to default "no write"
#define MAP_ROM_OFFSET 0x4000 // A15 is the switch! A15 = 0x4000*2 (word-pointer multiplicator)
#define FW_OFFSET 0x5000 // A15 is the switch! A15+A13 = 0x5000*2 (word-pointer multiplicator)
#define IDE_CONFIG_OFFSET 0x6000 // A15+A14 is the switch! A15+A14 = 0x6000*2 (word-pointer multiplicator)
#define CONFIG_BITS_OFFSET 0x7000 // A15+A14+A13 is the switch! A15+A14+A13 = 0x7000*2 (word-pointer multiplicator)
#define SIZE512K 524288
#define SIZE256K 262144
#define NO_BOARD_CODE 20
#define MAPROM_ACTIVATED 5


/* Function prototypes */

BOOL WriteKick(char *);
BOOL WriteExt(char *, ULONG);
BOOL CopyKick(void);
void WriteMap(UWORD val);
UWORD ReadMap(void);
UWORD ReadFW(void);
void WriteWaits(UWORD val);
UWORD ReadWaits(void);
void WriteConfigBits(UWORD val);
UWORD ReadConfigBits(void);
ULONG* LocateIDEBoard(void);
ULONG SizeOf68030TKV2(void);
ULONG SizeOfDickeOlga(void);
BOOL is68EC020TK(void);
ULONG SizeOfMemWithConfigBits(void);
ULONG GetFileSize(BPTR, char *);
BOOL Vbr2Fast(void);
BOOL isKick13(void);
ULONG StrLen(UBYTE *);
BOOL readCommandLine(int argc, char *argv[]);
void StrCopy(UBYTE* a, UBYTE* b);
void CleanUp(void);
ULONG hex2int(char *a);

__saveds __asm void DoReset(register __d0 UWORD fuses, register __a1 UWORD* board);
//__asm void ColdReboot(void);
__asm void SetVBR(register __a1 APTR vbr);
__asm ULONG GetVBR(void);


extern struct ExecBase *SysBase;



/* The fun starts here */

int main(int argc, char *argv[]) {

	UWORD fuseRead = 0,firmware=0;
	UWORD waitStateRead = 0;
	UWORD configBits =0;
	BOOL success = FALSE;
	BOOL need_reset = FALSE;
	UWORD* board = NULL;
	ULONG vbr;
	int returnCode=0;
	UWORD attnflags = SysBase->AttnFlags;
		
	if(readCommandLine(argc,argv)){	
		
			/*
			//for debugging
			printf("Command line parser:\n");
			printf("\tquiet: %d\n",quiet);
			printf("\tforce: %d\n",force);
			printf("\tstatus: %d\n",status);
			printf("\tremoveFuses: %d\n",removeFuses);
			printf("\tvbr2fast %d\n",vbr2fast);
			printf("\tkickrom: %d\n",kickrom);
			printf("\tsetWaitState: %d\n",setWaitState);
			printf("\tsetFuseMap: %d\n",setFuseMap);
			printf("\tfuseMap: %d\n",fuseMap);
			printf("\twaitState: %d\n",waitState);
			printf("\tkickFile: %s\n",kickFile);
			printf("\textFile: %s\n",extFile);
			printf("\tdiagFile: %s\n",diagFile);
			CleanUp();
			return 0;
			*/
			if(status && !quiet) {
   				printf(VSTRING);
   			}
						
			//vbr2Fast works on every supported CPU			
			if(vbr2fast) {
				//now check that we have a 68010 cpu at least
				if (attnflags & (AFF_68010 | AFF_68020 | AFF_68030 |AFF_68040 | 0x80)){ //0x80 is for 68060
					Vbr2Fast();
				}
			}

			board = (UWORD*) LocateIDEBoard();

			if(board){
				fuseRead = ReadMap();
				firmware = ReadFW();
			}
			else{
				fuseRead = 1;
				firmware = 0;
			}

			if(fuseRead == 1){
				if(!quiet) {
			    	printf("No compatible board found!\n");
			    }
			    CleanUp();
			    return(NO_BOARD_CODE); //high error!
			}			    
			

			if(status) {

    			if(!quiet) {
    				printf("Firmware version = %04d\n", firmware);
    				printf("Fuse Map = $%04lx\n", fuseRead);
    				if(firmware == 0 & fuseRead ==0){
    					printf("YOU HAVE A FIRMWARE WITHOUT VERSION-STRING SUPPORT!\n");
    			  }
    				vbr = GetVBR();
    				printf("VBR = $%08lx.\n", vbr);
						waitStateRead = ReadWaits();
						if(waitStateRead<=MAX_WAIT){
							printf("IDE waitstates = %d\n",waitStateRead);
						}
						configBits = ReadConfigBits();
						if(configBits!=1){
							if(is68030TKV2){
								printf("68030TKV2 config bits = %lx\n",configBits);
							}
							if(isFatOlga){
								printf("\"Dicke Olga\" config bits = %lx\n",configBits);
							}
						}
    			}
    			if(fuseRead & ENABLE_F8_READ_ENABLE){
    			    returnCode = MAPROM_ACTIVATED; //warn on MapROM enabled
    			}
				success = FALSE;
				need_reset = FALSE;
			}
			
			if(setWaitState){
				WriteWaits(waitState);
			}
			
			//now check that we have a 68020 cpu at least
			if (!(attnflags & (AFF_68020 | AFF_68030 |AFF_68040 | 0x80))){ //0x80 is for 68060
				if(!quiet) printf("MapRom functions do not work with an onboard CPU.\n");
				return (0);
			}

			if(removeFuses) {
				WriteMap(0);
				if(board){
					board+=MAP_ROM_OFFSET;
					DoReset(0,board);
				}
			}
			
			//kickrom has to be the first call, otherwise I cannot add a diagrom , which is needed for CDTV/A590
			if(kickrom) {
				if(!(fuseRead & ENABLE_F8_READ_ENABLE) || force == TRUE) {
					if(force | is68030TKV2 | isFatOlga |(is68EC020 && firmware>= SERIAL_TK68EC020_DIFFERENT_BITMAP)){
						WriteMap(fuseRead & ~(ENABLE_F8_WRITE_PROTECT | ENABLE_E0_WRITE_PROTECT));
					}
					success = CopyKick();
					fuseMap = (ENABLE_F8_READ_ENABLE |ENABLE_F8_WRITE_PROTECT | ENABLE_E0_READ_ENABLE | ENABLE_E0_WRITE_PROTECT);
					need_reset = is68030TKV2 || isFatOlga; // a 68030TKV2 or Dicke Olga needs a reset, because we just removed 2 MB from the memory
				}
				else {
					if(!quiet) printf("Kickstart already mapped.\n");
					returnCode =MAPROM_ACTIVATED; //warn on MapROM enabled
				}
			}
			
			if(kickFile !=NULL) {
				if(!(fuseRead & ENABLE_F8_READ_ENABLE) || force == TRUE) {
					if(force | is68030TKV2 | isFatOlga |(is68EC020 && firmware>= SERIAL_TK68EC020_DIFFERENT_BITMAP)){
						WriteMap(fuseRead & ~ENABLE_F8_WRITE_PROTECT);
					}
					success = WriteKick(kickFile);
					fuseMap = (ENABLE_F8_READ_ENABLE |ENABLE_F8_WRITE_PROTECT);
					need_reset = TRUE;
				}
				else {
					if(!quiet) printf("Kickstart already mapped.\n");
					returnCode =MAPROM_ACTIVATED; //warn on MapROM enabled
				}
			}

			if(extFile != NULL) {
				if(!(fuseRead & ENABLE_E0_READ_ENABLE) || force == TRUE) {
					if(force | is68030TKV2 | isFatOlga |(is68EC020 && firmware>= SERIAL_TK68EC020_DIFFERENT_BITMAP)){
						WriteMap(fuseRead & ~ENABLE_E0_WRITE_PROTECT);
					}
					success = WriteExt(extFile,0xe00000);
					fuseMap = (ENABLE_E0_READ_ENABLE | ENABLE_E0_WRITE_PROTECT);
					need_reset = TRUE;
				}
				else {
					if(!quiet) printf("EXTROM already mapped.\n");
					returnCode =MAPROM_ACTIVATED; //warn on MapROM enabled
				}
			}

			if(diagFile != NULL) {
				if(!(fuseRead & ENABLE_E0_READ_ENABLE) || force == TRUE) {
					if(force | is68030TKV2 | isFatOlga |(is68EC020 && firmware>= SERIAL_TK68EC020_DIFFERENT_BITMAP)){
						WriteMap(fuseRead & ~ENABLE_E0_WRITE_PROTECT);
					}
					success = WriteExt(diagFile,0xf00000);
					fuseMap = (ENABLE_E0_READ_ENABLE | ENABLE_E0_WRITE_PROTECT);
					need_reset = TRUE;
				}
				else {
					if(!quiet) printf("DIAGROM already mapped.\n");
					returnCode =MAPROM_ACTIVATED; //warn on MapROM enabled
				}
			}

			if(setFuseMap){
				fuseMap &= (ENABLE_F8_READ_ENABLE |ENABLE_F8_WRITE_PROTECT | ENABLE_E0_READ_ENABLE | ENABLE_E0_WRITE_PROTECT);
				success = TRUE;
				need_reset = FALSE;
			}


	}

	if(success) {
		//WriteMap(fuseMap);
		/* Reset Amiga */
		if(need_reset) {
				if(board){
					board+=MAP_ROM_OFFSET;
					//printf("Writing Fuse Map = $%04lx to $%04lx\n", fuseMap, board);
					DoReset(fuseMap,board);
				}
		}
		else{
			WriteMap(fuseMap);
		}
	}
	
  CleanUp();
	return(returnCode);
}

	/*** Commandline parser ***/
BOOL readCommandLine(int argc, char *argv[]){
	struct RDArgs *args;
	int i;
	if(isKick13()){
		if(argc==1){ //first argument is the command "maprom" itself!
			printf("Usage: maprom <options>\nOptions (with space between optionand argument!):%s\n",CMD_TEMPLATE);
			return FALSE;
		}
		for(i=1;i<argc; i++){
			if(!stricmp(argv[i],"quiet")){
				quiet =TRUE;	
			}
			if(!stricmp(argv[i],"force")){
				force =TRUE;	
			}
			if(!stricmp(argv[i],"status")){
				status =TRUE;	
			}		
			if(!stricmp(argv[i],"remove")){
				removeFuses =TRUE;	
			}		
			if(!stricmp(argv[i],"kickrom")){
				kickrom =TRUE;	
			}		
			if(!stricmp(argv[i],"vbr2fast")){
				vbr2fast =TRUE;	
			}	
			//now the "complex things"	
			if(!stricmp(argv[i],"kickfile")){ //length of "kickfile" : 8
				if(strlen(argv[i])==8){
					if((i+1)<argc){ // we have a following argument
						i++;
						kickFile = AllocMem(strlen(argv[i])+1, MEMF_PUBLIC|MEMF_CLEAR);
						StrCopy((STRPTR) argv[i],kickFile);
					}
				}
				else{
					//I'm to lazy to programm ':' and '=' handling in Kick1.3
				}
			}		
			if(!stricmp(argv[i],"extfile")){ //length of "extfile" : 7
				if(strlen(argv[i])==7){
					if((i+1)<argc){ // we have a following argument
						i++;
						extFile = AllocMem(strlen(argv[i])+1, MEMF_PUBLIC|MEMF_CLEAR);
						StrCopy((STRPTR) argv[i],extFile);
					}
				}
				else{
					//I'm to lazy to programm ':' and '=' handling in Kick1.3
				}
			}		

			if(!stricmp(argv[i],"diagfile")){ //length of "diagfile" : 8
				if(strlen(argv[i])==8){
					if((i+1)<argc){ // we have a following argument
						i++;
						diagFile = AllocMem(strlen(argv[i])+1, MEMF_PUBLIC|MEMF_CLEAR);
						StrCopy((STRPTR) argv[i],diagFile);
					}
				}
				else{
					//I'm to lazy to programm ':' and '=' handling in Kick1.3
				}
			}	
			
			if(!stricmp(argv[i],"waitstate")){ //length of "waitstate" : 9
				if(strlen(argv[i])==9){
					if((i+1)<argc){ // we have a following argument
						i++;
						setWaitState = TRUE;
						waitState = atoi(argv[i]);
					}
				}
				else{
					//I'm to lazy to programm ':' and '=' handling in Kick1.3
				}
			}	
			
			if(!stricmp(argv[i],"fuse")){ //length of "fuse" : 4
				if(strlen(argv[i])==4){
					if((i+1)<argc){ // we have a following argument
						i++;
						setFuseMap = TRUE;
						fuseMap = hex2int(argv[i]);
					}
				}
				else{
					//I'm to lazy to programm ':' and '=' handling in Kick1.3
				}
			}	
		}
		return TRUE;
	}
	else{	
		if( (args = AllocDosObject(DOS_RDARGS, NULL)) ) {
			if( (ReadArgs(CMD_TEMPLATE, (LONG *) argvalues, args)) ) {
				if(argvalues[ARG_QUIET]) {
					quiet = TRUE;
				}
	
				if(argvalues[ARG_FORCE]) {
					force = TRUE;
				}
	
				if(argvalues[ARG_STATUS]) {		
					status =TRUE;
				}
	
				if(argvalues[ARG_REMOVE]) {
					removeFuses = TRUE;
				}
				if(argvalues[ARG_KICKROM]){
					kickrom = TRUE;
				}
				if(argvalues[ARG_VBR2FAST]){
					vbr2fast = TRUE;
				}
				if(argvalues[ARG_WAITSTATE]){
					setWaitState = TRUE;
					waitState = *argvalues[ARG_WAITSTATE];
				}
				if(argvalues[ARG_FUSE]){
					setFuseMap = TRUE;
					fuseMap = hex2int((char*)argvalues[ARG_FUSE]);
				}
	
				if(argvalues[ARG_KICKFILE]) {
					kickFile = AllocMem(strlen((STRPTR) argvalues[ARG_KICKFILE])+1, MEMF_PUBLIC|MEMF_CLEAR);
					StrCopy((STRPTR) argvalues[ARG_KICKFILE],kickFile);
				}
				if(argvalues[ARG_EXTFILE]) {
					extFile = AllocMem(strlen((STRPTR) argvalues[ARG_EXTFILE])+1, MEMF_PUBLIC|MEMF_CLEAR);
					StrCopy((STRPTR) argvalues[ARG_EXTFILE],extFile);
	
				}
				if(argvalues[ARG_DIAGFILE]) {
					diagFile = AllocMem(strlen((STRPTR) argvalues[ARG_DIAGFILE])+1, MEMF_PUBLIC|MEMF_CLEAR);
					StrCopy((STRPTR) argvalues[ARG_DIAGFILE],diagFile);
					
				}
	
	
				FreeArgs(args);
				FreeDosObject(DOS_RDARGS, args);
				return TRUE;
			}
			else{
				FreeDosObject(DOS_RDARGS, args);
			}
		}
	}	
	return FALSE; //drop here if something went wrong...
}


void CleanUp(void){
	if(kickFile) FreeMem(kickFile,strlen(kickFile)+1);
	if(extFile) FreeMem(extFile,strlen(extFile)+1);
	if(diagFile) FreeMem(diagFile,strlen(diagFile)+1);
		
}


/*
 * WriteKick - Write Kickstart image to maprom memory
 *
 * Parameters: filename - Pointer to filename string
 *
 * Returns:    TRUE on success, FALSE on error
 *
 */
BOOL WriteKick(char *filename) {
	APTR buffer = NULL;
	ULONG filesize = 0;
	ULONG *ptr;
	ULONG *kickmem = (ULONG *) 0xf80000;
	//UWORD *COLOR00 = (UWORD *) 0xdff180;
	ULONG signature;
	BOOL success = FALSE;
	BPTR fh = 0;
	LONG bytes_read;
	int i;
	
	fh = Open(filename, MODE_OLDFILE);
	if(fh == 0) {
		printf("Error: Failed to open Kickstart file.\n");
		return FALSE;
	}

	filesize = GetFileSize(fh, filename);
	if(filesize == -1) return FALSE;

	if(filesize == SIZE256K || filesize == SIZE512K) {
		buffer = AllocMem(filesize, MEMF_PUBLIC|MEMF_CLEAR);
		if(buffer != NULL) {
			bytes_read = Read(fh, buffer, filesize);
			if(bytes_read == SIZE256K || bytes_read == SIZE512K) {
				signature = ((ULONG *) buffer)[0];

				if(signature == 0x11114ef9 || signature == 0x11144ef9) {
					if(!quiet) printf("Mapping Kickstart image from file '%s' ... \n", filename);

					Disable();
					ptr = (ULONG *) buffer;
					for(i = 0; i < filesize; i+=4) {
						//*COLOR00 = *ptr;
						*kickmem++ = *ptr++;
					}

					/* Write 256k image twice */
					if(filesize == SIZE256K) {
						ptr = (ULONG *) buffer;
						for(i = 0; i < filesize; i+=4) {
							//*COLOR00 = *ptr;
							*kickmem++ = *ptr++;
						}
					}
					Enable();

					if(!quiet) printf("Ok!\n");

					success = TRUE;
				}
				else {
					printf("Error: File does not look like a Kickstart image.\n");
				}
			}
			else {
				printf("Read error on file.\n");
			}

			FreeMem(buffer, filesize);
		}
		else {
			printf("Error: Failed to allocate buffer\n");
		}
	}
	else {
		printf("Error: File has illegal size\n");
	}

	Close(fh);

	return success;
}



/*
 * WriteExt - Write ExtROM image to maprom memory
 *
 * Parameters: 	filename - Pointer to filename string 
 * 				addr - Address to write to
 *
 * Returns:    TRUE on success, FALSE on error
 *
 */
BOOL WriteExt(char *filename, ULONG addr) {
	APTR buffer = NULL;
	ULONG filesize = 0;
	ULONG *ptr;
	ULONG *kickmem = (ULONG *) addr;
	BOOL success = FALSE;
	BPTR fh = 0;
	int i;

	fh = Open(filename, MODE_OLDFILE);
	if(fh == 0) {
		printf("Error: Failed to open EXT/DIAG ROM file.\n");
		return FALSE;
	}

	filesize = GetFileSize(fh, filename);
	if(filesize == -1) return FALSE;

	if(filesize <= SIZE512K) {
		buffer = AllocMem(filesize, MEMF_PUBLIC|MEMF_CLEAR);
		if(buffer != NULL) {
			Read(fh, buffer, filesize);

			if(!quiet) printf("Mapping EXT/DIAG ROM image from file '%s' to 0x%08lx ... \n", filename,addr);

			Disable();
			ptr = (ULONG *) buffer;
			for(i = 0; i < filesize; i+=4) {
				*kickmem++ = *ptr++;
			}
			Enable();

			success = TRUE;
			FreeMem(buffer, filesize);
		}
		else {
			printf("Error: Failed to allocate buffer\n");
		}
	}
	else {
		printf("Error: File is too large.\n");
	}

	Close(fh);

	return success;
}


/*
 * CopyKick - Copy Kickstart image from $F80000 to maprom memory
 *
 * Parameters: -
 *
 * Returns:    TRUE on success, FALSE on error
 *
 */
BOOL CopyKick(void) {
	ULONG *kickptr = (ULONG *) 0xf80000;
	ULONG *kickmem = (ULONG *) 0xf80000;
	ULONG *extptr = (ULONG *) 0xe00000;
	ULONG *extmem = (ULONG *) 0xe00000;
	ULONG *diagptr = (ULONG *) 0xf00000;
	ULONG *diagmem = (ULONG *) 0xf00000;
	//UWORD *COLOR00 = (UWORD *) 0xdff180;
	int i;
	
	if(!quiet) printf("Mapping Kickstart ROM ... \n");

	Disable();
	for(i = 0; i < SIZE512K; i+=4) {
		//*COLOR00 = *kickptr;
		*kickmem++ = *kickptr++;
		*extmem++ = *extptr++;	
		*diagmem++ = *diagptr++;	
	}
	Enable();

	return TRUE;
}


/*
 * GetFileSize - Get size of a file (Kick 1.3 compatible)
 *
 * Parameters: fh       - File handle (BPTR)
 *             filename - File name (char *)
 *
 * Returns:    filesize (ULONG)
 *
 */
ULONG GetFileSize(BPTR fh, char *filename) {
	struct FileInfoBlock *fib;
	BPTR lock;
	ULONG filesize = -1L;

	fib = AllocMem(sizeof(struct FileInfoBlock), MEMF_PUBLIC|MEMF_CLEAR);
	if(fib == NULL) {
		printf("Error: Failed to alloc FileInfoBlock\n");
		Close(fh);
		return FALSE;
	}

	lock = Lock(filename, ACCESS_READ);
	if(lock) {
		if(Examine(lock, fib) != FALSE) {
			filesize = fib->fib_Size;
		}
		else {
			printf("Error: Examine() call failed\n");
		}
		UnLock(lock);
	}
	else {
		printf("Error: Lock() call failed\n");
	}
	FreeMem(fib, sizeof(struct FileInfoBlock));

	return filesize;
}


/*
 * WriteMap - Enable memory mapping
 *
 * Parameters: -
 *
 * Returns:    -
 *
 */
void WriteMap(UWORD map) {
	UWORD *enable = (UWORD*)LocateIDEBoard(); //cast to uword because we have to write to D31-D28 only once!
	ULONG mem_size = 0;
	ULONG *test_address =0;
	UWORD val = 0;
	ULONG test1 = 0;
	ULONG test2 = 0;
	char* boardName;
	BOOL has128MB = FALSE; 
	BOOL has64MB = FALSE; 
	if(!enable)
		return;	
	enable+=MAP_ROM_OFFSET;
	if(!quiet ) printf("Writing magic word 0x%04x to address 0x%08lx... \n",map, enable);
	//determine board name and size
	if(is68030TKV2){
		mem_size = SizeOf68030TKV2(); //this is not-zero if a board with config-bits is found
		boardName = "68030TKV2"; 		
	}
	if(isFatOlga){
		mem_size = SizeOfDickeOlga(); //this is not-zero if a board with config-bits is found
		boardName = "\"Dicke Olga\""; 		
	}
	
	if(is68030TKV2 | isFatOlga){
		/*
		Now a special trick to enable MAP-ROM on 68EC030_CPUs on the Dicke Olga/68030TKV2-board:
		I need to know the size if the mem card to activate a special bit, which toggles the mapping to $43Fxxxxx (64MB) or $47Fxxxxx (128MB)
		*/
		
		if(mem_size > 0x04000000){ //more than 64MB
			has128MB = TRUE;
		}
		else if(mem_size > 0x01000000){ //more than 16MB
			has64MB = TRUE;
		}
		else{//arg! we are in kick1.3! we have to test if there is 64 or 128MB manually by writing some testdata
			test_address = 0x40000000;	//base address
			test_address[0x01000000] = 0xF00DBEEF; //write a test word to 128MB: because we are longwords we have to divide the offset by 4!
			test_address[0] = 0xBEEFADD8; //write a test word
			test1 = test_address[0];		
			test2 = test_address[0x01000000]; //whats above 64MB?
			if(test1 == 0xBEEFADD8){//found mem
				if(test1==test2){ //mirroring = 64MB
					has64MB = TRUE;
				}
				else if(test2 == 0xF00DBEEF){ // found 128MB
					has128MB = TRUE;
				}
				else{ //rubbish at 2nd mem location
					has64MB = TRUE;
				}
			}
			else{
				if(!quiet ) printf("Found a 68030TKV2 with no RAM?!?\n");
			}
		}
		
		
		if(has128MB){ //more than 64MB
			if(!quiet ) printf("Found a %s with 128MB\n",boardName);
			val = ReadConfigBits(); //read old value
			WriteConfigBits(val|0x8000); //set config bit(3)
		}
		else if(has64MB){ //more than 16MB
			if(!quiet ) printf("Found a %s with 64MB\n", boardName);
			val = ReadConfigBits(); //read old value
			WriteConfigBits(val&0x7FFF); //unset config bit(3)
		}
	}

	*enable = map;
}


/*
 * ReadMap - Print memory mapping
 *
 * Parameters: -
 *
 * Returns:    the parameters from the map rom register
 *
 */
UWORD ReadMap(void) {
	UWORD retVal=0;
	UWORD *enable = (UWORD*)LocateIDEBoard(); //cast to uword because we have to write to D31-D28 only once!
	if(!enable)
		return 1;	 //-1 is no UWORD
	
	enable+=MAP_ROM_OFFSET;
	retVal = *enable;
	retVal = retVal & (ENABLE_F8_READ_ENABLE| ENABLE_F8_WRITE_PROTECT | ENABLE_E0_READ_ENABLE |ENABLE_E0_WRITE_PROTECT);
	
	return retVal;
}

/*
 * ReadFW - Print firmware version string
 *
 * Parameters: -
 *
 * Returns:    the firmware version string
 *
 */
UWORD ReadFW(void) {
	UWORD retVal=0;
	UWORD *enable = (UWORD*)LocateIDEBoard(); //cast to uword because we have to write to D31-D28 only once!
	if(!enable)
		return  1;	 //-1 is no UWORD
	
	enable+=FW_OFFSET;
	retVal = *enable;
	retVal = retVal>>12; //shift 12 bits to the right to get the firmware to the right position
	
	return retVal;
}

/*
 * WriteWaits - Write IDE waitstates
 *
 * Parameters: -
 *
 * Returns:    -
 *
 */
void WriteWaits(UWORD map) {
	UWORD *enable = (UWORD*)LocateIDEBoard(); //cast to uword because we have to write to D31-D28 only once!
	if(!enable)
		return;	
	enable+=IDE_CONFIG_OFFSET;
	map = map & MAX_WAIT; //allow only last 4 bits
	map = map<<12; //shift 12 bit to put the values to D31-D16
	*enable = map;
}


/*
 * ReadWaits - read ide waitstate setting
 *
 * Parameters: -
 *
 * Returns:    the parameters from the map rom register
 *
 */
UWORD ReadWaits(void) {
	UWORD retVal=0;
	UWORD *enable = (UWORD*)LocateIDEBoard(); //cast to uword because we have to write to D31-D28 only once!
	if(!enable)
		return  MAX_WAIT+1;	 //-1 is no UWORD
	
	enable+=IDE_CONFIG_OFFSET;
	retVal = *enable;
	retVal = retVal >>12;
	retVal = retVal & MAX_WAIT; //mask unset bits
	return retVal;
}

/*
 * WriteConfigBits - Write config bits for 68030TKV2
 *
 * Parameters: -
 *
 * Returns:    -
 *
 */
void WriteConfigBits(UWORD map) {
	UWORD *enable = (UWORD*)LocateIDEBoard(); //cast to uword because we have to write to D31-D28 only once!
	ULONG mem_size = SizeOfMemWithConfigBits(); //this is not-zero if a board with config-bits is found
	
	if(!mem_size | !enable)
		return;	
		
	enable+=CONFIG_BITS_OFFSET;
	if(!quiet ) printf("Writing config bits = $%04lx to $%04lx, boardsize: $%04lx\n", map, enable,mem_size);
	*enable = map;
}


/*
 * ReadWaits - read config bit setting
 *
 * Parameters: -
 *
 * Returns:    the parameters from the config bit register
 *
 */
UWORD ReadConfigBits(void) {
	UWORD retVal=0;
	ULONG mem_size = SizeOfMemWithConfigBits(); //this is not-zero if a board with config-bits is found
	UWORD *enable = (UWORD*)LocateIDEBoard(); //cast to uword because we have to write to D31-D28 only once!
	
	if(!mem_size | !enable)
		return  1;	 //-1 is no UWORD
	
	enable+=CONFIG_BITS_OFFSET;
	retVal = *enable;
	if(!quiet ) printf("Reading config bits = $%04lx from $%04lx, boardsize: $%04lx\n", retVal, enable,mem_size);
	return retVal;
}

/*
** **********************************************
** LocateIDEBoard()
** **********************************************
**
** Locates IDE Controller hardware
**
*/
ULONG* LocateIDEBoard(void)
{
	ULONG* board= NULL; // Board not found (yet)
	struct Library *ExpansionBase= NULL;
 

	if ( ExpansionBase = OpenLibrary( "expansion.library", 0L ) ) {

		struct ConfigDev *cfg = NULL;

		// *** Find IDE board

		while ((cfg = FindConfigDev(cfg, -1, -1)) != NULL) {

			if ((cfg->cd_Rom.er_Manufacturer == MANUFACTOR && cfg->cd_Rom.er_Product == PRODUCT) ||
			cfg->cd_Rom.er_Manufacturer == MANUFACTOR_A1K && cfg->cd_Rom.er_Product == PRODUCT_MATZE_IDE)
			{
				//find correct serial number
				if(cfg->cd_Rom.er_SerialNumber==SERIAL) {
					board = cfg->cd_BoardAddr; // Get board base adr.
					break;
				}
			}
		}

		CloseLibrary( ExpansionBase );
	}

	is68030TKV2 = SizeOf68030TKV2()>0; // see if we have a 68030TKV2
	isFatOlga = SizeOfDickeOlga()>0; // see if we have a Dicke Olga
	is68EC020 = is68EC020TK(); //see if we have a 68EC020TK
	return( board ); // Return board ptr (or NULL)
}

/*
** **********************************************
** SizeOf68030TKV2()
** **********************************************
**
** Finds the memory size of the turbo board.
Returns 64MB or 128MB or NULL if not detected
**
*/
ULONG SizeOf68030TKV2(void)
{
    ULONG size = 0;
    struct Library *ExpansionBase= NULL;
 

    if ( ExpansionBase = OpenLibrary( "expansion.library", 0L ) ) {

        struct ConfigDev *cfg;


		if (cfg = FindConfigDev( NULL, MANUFACTOR_A1K, PRODUCT_TK68030V2 )) 
		{
				if(cfg->cd_Rom.er_SerialNumber>=SERIAL_TK68030V2){ 
					size = cfg->cd_BoardSize; 
				}
		}	
		

        CloseLibrary( ExpansionBase );
    }

    return size ; // Return size (or NULL)
}

/*
** **********************************************
** is68EC020TK()
** **********************************************
**
** Finds the 68EC020TK
**
*/
BOOL is68EC020TK(void)
{
    BOOL found = FALSE;
    struct Library *ExpansionBase= NULL;
 

    if ( ExpansionBase = OpenLibrary( "expansion.library", 0L ) ) {

        struct ConfigDev *cfg;


		if (cfg = FindConfigDev( NULL, MANUFACTOR_A1K, PRODUCT_TK68EC020 )) 
		{
			found =TRUE;
		}	
		

        CloseLibrary( ExpansionBase );
    }

    return found ; 
}


/*
** **********************************************
** SizeOfDickeOlga()
** **********************************************
**
** Finds the memory size of the turbo board.
Returns 64MB or 128MB or NULL if not detected
**
*/
ULONG SizeOfDickeOlga(void)
{
    ULONG size = 0;
    struct Library *ExpansionBase= NULL;
 

    if ( ExpansionBase = OpenLibrary( "expansion.library", 0L ) ) {

        struct ConfigDev *cfg;


		if (cfg = FindConfigDev( NULL, MANUFACTOR_A1K, PRODUCT_DICKE_OLGA )) 
		{
				if(cfg->cd_Rom.er_SerialNumber>=SERIAL_DICKE_OLGA){ 
					size = cfg->cd_BoardSize; 
				}
		}
		

        CloseLibrary( ExpansionBase );
    }

    return size ; // Return size (or NULL)
}

/*
** **********************************************
** SizeOfMemWithConfigBits()
** **********************************************
**
** Finds the memory size of the turbo board.
Returns 64MB or 128MB or NULL if no config bit-compatible board is detected
**
*/
ULONG SizeOfMemWithConfigBits(void)
{
	ULONG mem_size=NULL;
	if(is68030TKV2){
		mem_size = SizeOf68030TKV2(); //this is not-zero if a board with config-bits is found
	}
	if(isFatOlga){
		mem_size = SizeOfDickeOlga(); //this is not-zero if a board with config-bits is found
	}
	return mem_size ; // Return size (or NULL)
}
/*
 * Vbr2Fast - Move VBR to Fast Memory
 *
 * Parameters: -
 *
 * Returns:    -
 *
 */
BOOL Vbr2Fast(void) {
	ULONG vbr = 0L;
	APTR newvbr;

	/* Check if VBR has already been moved */
	vbr = GetVBR();
	if(vbr != 0L) {
		if(!quiet) printf("VBR has already been moved to $%08lx.\n", vbr);
	}
	else {
		if( (newvbr = AllocMem(1032L, MEMF_FAST|MEMF_CLEAR)) ) {
			newvbr = (APTR)(((ULONG) newvbr + 4L) & 0xFFFFFFFC);
			Disable();
			CopyMemQuick(0L, newvbr, 1024);
			SetVBR(newvbr);
			Enable();

			vbr = GetVBR();
			if(!quiet) printf("VBR moved to $%08lx.\n", vbr);
		}
		else {
			if(!quiet) printf("Failed to alloc 1k of Fast Memory.\n");
		}
	}

	return FALSE;
}
ULONG StrLen(UBYTE *string)
{
    ULONG x = 0L;
    while (string[x++]);
    return(x);
}
void StrCopy(UBYTE* a, UBYTE* b){
	while(*b++=*a++);
}
BOOL isKick13(void){
	struct Library *ExpansionBase=NULL;
	BOOL isKick13=FALSE;
 	ExpansionBase=OpenLibrary("expansion.library",0L);
  if (ExpansionBase){
      //version check
      if(ExpansionBase->lib_Version <36)
      	isKick13 = TRUE;
      CloseLibrary(ExpansionBase);
  }
  return isKick13;
}
ULONG hex2int(char *a)
{
    int i,len=0,start=0;
    ULONG val = 0;
		
		//scan for a 'x' or '$'
		
		i=0;
		while(a[i]){
			if(a[i]=='x' || a[i]=='X' || a[i]=='$'){
				start=i+1;
				break;
			}
			i++;
		}
		
		if(!a[i]){ // no hex-identifier found: return as int!
			return (ULONG) atoi(a);
		}
		
		//start at the first character after the hex-key
		len=start;
		//determine length
		while(a[len])
			len++;
    for(i=start;i<len;i++){
       if(a[i]>=48 &&a[i] <= 57) //0..9
        val += (a[i]-48)*(1<<(4*(len-1-i)));
       else if(a[i]>=65 &&a[i] <= 70) //A..F
        val += (a[i]-55)*(1<<(4*(len-1-i)));
       else if(a[i]>=97 &&a[i] <= 102) //a..f
       	val += (a[i]-87)*(1<<(4*(len-1-i)));
       
		}
    return val;
}
