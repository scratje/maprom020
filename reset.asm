	SECTION	"reset_code",CODE_C

	MACHINE	MC68020

	include	"lvo/exec_lib.i"

	XDEF	_DoReset
	XDEF	_ResetCode

	XDEF	_GetVBR
	XDEF	_SetVBR

_GetVBR:
	movem.l	a5/a6,-(sp)
	lea.l	_readvbr,a5
	move.l	$4.w,a6
	jsr	_LVOSupervisor(a6)
	movem.l	(sp)+,a5/a6
	rts

_readvbr:
	movec	VBR,d0
	rte

_SetVBR:
	movem.l	d1/a5/a6,-(sp)
	lea.l	_writevbr,a5
	move.l	$4.w,a6
	move.l	a1,d1
	jsr	_LVOSupervisor(a6)
	movem.l	(sp)+,d1/a5/a6
	rts

_writevbr:
	movec	d1,VBR
	rte


;fuses in d0
;board address including maprom offset in a1
_DoReset:		
	lea.l	_ResetCode,a5
	moveq	#0,d7
	move.l	$4.w,a6

	jsr	_LVODisable(a6)
	jsr	_LVOSupervisor(a6)	; Run reset routine in supervisor mode
	
	rts				; Pretty useless :-)

	cnop	0,4

_ResetCode:
	move.l	#$8,d6
	movec	d6,cacr			; Flush and disable 68020 I-Cache

	move.w 	#$2700,sr		; Disable all interrupts
	move.w 	#$7fff,$dff09a		; Disable chipset interrupts
	move.w 	#$7fff,$dff09c		; Clear pending interrupts
	move.w 	#$7fff,$dff096		; Disable chipset DMA
	move.b 	#$7f,$bfed01		; Disable CIAB interrupts
	move.b 	#$7f,$bfdd00		; Disable CIAA interrupts
	move.w d0,(a1) ; set the fuses
		
	clr.l 	d0
	movec 	d0,vbr			; Set VBR to $0
	
	clr.l	$4.w			; clear execbase

	lea.l	$1000000,a0
	sub.l	-$14(a0),a0
	move.l	4(a0),a0
	subq.l	#2,a0
	reset	
	jmp 	(a0)			; This works thanks to 68K prefetch
